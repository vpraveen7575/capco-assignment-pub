import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router
import { TableComponent } from './table/table.component';
import { SimpleTableComponent } from './simple-table/simple-table.component';

 // sets up routes constant where you define your routes
const routes: Routes = [
    { path: 'with-pagination', component: TableComponent },
    { path: 'without-pagination', component: SimpleTableComponent },
    { path: '',   redirectTo: '/with-pagination', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
