import { Component, OnInit } from '@angular/core';
import { TableService } from './table.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  tableData: any = [{}];
  displayData: any = [{}];
  objectKeys = Object.keys;
  objectValues = Object.values;

  recordsPerPage = 10;
  pageNum = 1;
  pageNumbers: number[];



  constructor(private _table: TableService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this._table.getData()
      .subscribe((data) => {
        this.tableData = data;
        this.paginate();
      });
  }

  paginate() {
    const totalPages = Math.ceil(this.tableData.length / this.recordsPerPage);

    if (totalPages < this.pageNum) {
      this.pageNum = totalPages;
    }

    this.pageNumbers = Array(totalPages).fill(1);

    this.displayData = this.tableData.slice((this.pageNum - 1) * this.recordsPerPage, this.pageNum * this.recordsPerPage);
  }

}
