import { Component, OnInit } from '@angular/core';
import { TableService } from '../table/table.service';

@Component({
  selector: 'app-simple-table',
  templateUrl: './simple-table.component.html',
  styleUrls: ['./simple-table.component.scss']
})
export class SimpleTableComponent implements OnInit {

  tableData: any = [{}];
  objectKeys = Object.keys;
  objectValues = Object.values;



  constructor(private _table: TableService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this._table.getData()
      .subscribe((data) => {
        this.tableData = data;
      });
  }

}
